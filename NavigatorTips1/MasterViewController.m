//
//  MasterViewController.m
//  NavigatorTips1
//
//  Created by See.Ku on 2014/03/01.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"

@interface MasterViewController ()
{
//    NSMutableArray *_objects;

	//		最後に使用したセグエのidentifier
	
	NSString* _segueMemory;
}
@end

@implementation MasterViewController

- (void)configSettings:(id)sender
{
	//		モーダルビューを開く
	
	id vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Config"];
	[self presentViewController:vc animated:YES completion:nil];
}

- (void)awakeFromNib
{
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
	    self.clearsSelectionOnViewWillAppear = NO;
	    self.preferredContentSize = CGSizeMake(320.0, 600.0);
	}
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	//		初期化
	
	_segueMemory = @"detail1";

	//		戻るボタンを変更
	
	UIBarButtonItem* btn = [[UIBarButtonItem alloc] initWithTitle:@"Back"
															style:UIBarButtonItemStylePlain
														   target:nil
														   action:nil];
	self.navigationItem.backBarButtonItem = btn;
	
	//		Navigation Barにボタンを追加

	UIBarButtonItem* btn2 = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings-i"]
															 style:UIBarButtonItemStylePlain
															target:self
															action:@selector(configSettings:)];
	self.navigationItem.rightBarButtonItem = btn2;
	
	// Do any additional setup after loading the view, typically from a nib.
//	self.navigationItem.leftBarButtonItem = self.editButtonItem;
//
//	UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
//	self.navigationItem.rightBarButtonItem = addButton;
//	self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}
*/

#pragma mark - Table View

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

	NSDate *object = _objects[indexPath.row];
	cell.textLabel.text = [object description];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}
*/
/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	//		対象のセルがクリックされた時は、記憶しておいたセグエで遷移する
	
	UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
	if( [cell.reuseIdentifier isEqualToString:@"MemoryCell"] ) {
		[self performSegueWithIdentifier:_segueMemory sender:nil];
	}
	
	//		Storyboardで接続していない画面へ遷移

	if( [cell.reuseIdentifier isEqualToString:@"UnconnectedCell"] ) {
		id vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Unconnected"];
		[self.navigationController pushViewController:vc animated:YES];
	}
	
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
//        NSDate *object = _objects[indexPath.row];
//        self.detailViewController.detailItem = object;
//    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	//		セグエのidentifierを記憶しておく
	
	_segueMemory = [segue identifier];

//    if ([[segue identifier] isEqualToString:@"showDetail"]) {
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        NSDate *object = _objects[indexPath.row];
//        [[segue destinationViewController] setDetailItem:object];
//    }
}

@end
