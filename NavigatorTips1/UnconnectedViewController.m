//
//  UnconnectedViewController.m
//  NavigatorTips1
//
//  Created by See.Ku on 2014/03/01.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "UnconnectedViewController.h"

@interface UnconnectedViewController ()

@end

@implementation UnconnectedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
