//
//  DetailViewController.h
//  NavigatorTips1
//
//  Created by See.Ku on 2014/03/01.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
