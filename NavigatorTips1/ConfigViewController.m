//
//  ConfigViewController.m
//  NavigatorTips1
//
//  Created by See.Ku on 2014/03/03.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "ConfigViewController.h"

static BOOL g_switch;

@interface ConfigViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *configSwitch;

- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;
@end

@implementation ConfigViewController

- (IBAction)done:(id)sender
{
	//		設定した内容を保存して終了

	g_switch = _configSwitch.on;
	
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancel:(id)sender
{
	//		設定した内容を破棄して終了

	[self dismissViewControllerAnimated:YES completion:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

	_configSwitch.on = g_switch;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
