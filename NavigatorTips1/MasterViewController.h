//
//  MasterViewController.h
//  NavigatorTips1
//
//  Created by See.Ku on 2014/03/01.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
